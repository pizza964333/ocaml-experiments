

import Crypto.Hash.SHA256
import qualified Data.ByteString as B
import Data.ByteString (ByteString)


data Node = N Integer deriving Show

data Conh = Conh Int

hash :: [Integer] -> Integer
hash = undefined

w2b :: [Word64] -> ByteString
w2b (a:b) =

w2o :: Word64 -> [Word8]
w2o 0 = []
w2o a = fromInteger (a `mod` 256) : w2o (a `div` 256)
 
i2w :: Integer -> [Word64]
i2w a = (a `mod` (2^64)) : i2w (a `div` (2^64))

w2i :: [Word64] -> Integer
w2i [] = 0
w2i (a:b) = toInteger a + (w2i b) * (2^64)

randomNode :: IO Node
randomNode = do
  a <- randomRIO (2^32,2^48)
  return $ Node a

addNode :: Node -> Conh -> Conh
addNode a b = undefined

main = do
  a <- create
  b <- mapM_ (\_ -> randomNode) [0..9]
  let c = foldr (\a b -> addNode a b) a b
  print 123


