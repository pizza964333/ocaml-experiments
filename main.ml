
module BinaryNet = struct
  type elem = ElemT
  type branch = BranchT
  type ('a,'b,'c,'d) t =
    | Unit : ('a,'b,branch,elem) t
    | Leaf : 'l -> ('l,'a,branch,'b) t
    | Elem : 'e -> ('a,'e,'b,elem) t
    | Node : ('l,'e,'a,elem) t
           * ('l,'e,branch,'b) t ref
           * ('l,'e,branch,'c) t ref
           * ('l,'e,branch,'d) t ref -> ('l,'e,branch,'f) t
  let single a = Node (Elem a, ref Unit, ref Unit, ref Unit)
end;;



print_string "testing\n";;